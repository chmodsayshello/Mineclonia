---

name: "Bug Report"
about: "Use this for when something's broken."
labels:
 - bug
 - unconfirmed

---

##### What happened?

<!-- Describe what's wrong. -->

##### What did I expect?

<!-- Describe what should be happening instead -->

##### How to get it to happen

<!--
Write down exactly what you did to get the bug to happen
If you need more steps, just keep adding numbers. If you
don't need them all, delete the empty numbers.
-->
1. 
2. 
3. 

##### Environment

Mineclonia Version: <!-- Paste the version of Mineclonia here, if you know it. -->

<!--
Please refer to https://git.minetest.land/Mineclonia/Mineclonia/wiki/Reporting-Bugs
if you need help finding your Minetest version.
-->
Minetest Version:
